CREATE DATABASE `dinno_board`;

create user 'dinno'@'%' identified by 'dkagh1.';

GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY 'dkagh1.';
GRANT ALL PRIVILEGES ON *.* TO dinno@'%' IDENTIFIED BY 'dkagh1.';

FLUSH PRIVILEGES;

USE `dinno_board`;

CREATE TABLE `board` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `title` varchar(200) NOT NULL COMMENT '제목',
  `content` text NOT NULL COMMENT '내용',
  `read_cnt` int(11) NOT NULL DEFAULT 0 COMMENT '조회수',
  `register_id` varchar(100) NOT NULL COMMENT '작성자',
  `register_time` datetime DEFAULT NULL COMMENT '작성일',
  `update_time` datetime DEFAULT NULL COMMENT '수정일',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='게시판';

INSERT INTO `board` VALUES (1,'Dinnoit_Board','Test Board1',0,'DinnoIT','2022-03-09 22:51:27','2022-03-09 22:51:27');
INSERT INTO `board` VALUES (2,'Board Test','test',0,'Boarder','2022-03-09 22:51:46','2022-03-09 22:51:46');
INSERT INTO `board` VALUES (3,'Test Data','test',0,'Tester','2022-03-09 22:51:58','2022-03-09 22:51:58');
INSERT INTO `board` VALUES (4,'Test Data1','test',0,'Tester','2022-03-09 22:52:49','2022-03-09 22:52:49');
INSERT INTO `board` VALUES (5,'Test Data2','test',0,'tester','2022-03-09 22:53:06','2022-03-09 22:53:06');
