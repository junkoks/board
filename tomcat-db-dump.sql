CREATE DATABASE `bbs`;

USE `bbs`;

CREATE TABLE `bbs` (
  `bbsID` int(11) NOT NULL,
  `bbsTitle` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bbsDate` datetime DEFAULT NULL,
  `bbsContent` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bbsAvailable` int(11) DEFAULT NULL,
  PRIMARY KEY (`bbsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `bbs` VALUES (1,'abcaaaa','test','2022-06-23 17:39:57','abc',1),(2,'test','abc','2022-07-12 14:00:37','abcd',1);

CREATE TABLE `user` (
  `userID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `userPassword` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userName` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userGender` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userEmail` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` VALUES ('abc','1112','1111','남자','abc1@abc.com'),('test','test','test','남자','abc1@abc.com');
